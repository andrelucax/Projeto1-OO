1º Exercício Prático - Orientação a Objetos 2018.1 - UnB - Gama
=========================
André Lucas de Sousa Pinto - 17/0068251

## Instalação

1. Faça o clone deste projeto com git clone https://gitlab.com/andrelucax/Projeto1-OO.git
2. Entre na pasta do projeto e execute o comando ```$ make```
3. Rode o programa com o comando ```$ make run```
4. Execute as ações de acordo com o que é oferecido no menu

## Exemplos

**Exemplo 1: como abrir e salvar uma imagem do tipo PGM**
```
	[MENU INICIAL]
	1 (ENTER)  --  entra na função de leitura PGM
	
	[FUNCAO LEITURA]
	exemplo.pgm (ENTER)   --   caso a imagem não esteja na mesma pasta que o projeto colocar o link completo
	
	[SEGUNDO MENU]
	1 (ENTER)  --  entra na função de gravura
	
	[FUNCAO GRAVURA]
	nomeQueDesejaDar.pgm (ENTER)   --   a imagem será salva na mesma pasta que o projeto
	(ENTER)  --  a aplicação pede que de um enter para continuar
	
	[SEGUNDO MENU]
	0 (ENTER)  --  para sair do programa
	(ENTER)  --  a aplicação pede que de um enter para continuar
```
	
**Exemplo 2: como ler a mensagem escondida na PPM**
```
	[MENU INICIAL]
	2 (ENTER)  --  entra na função de leitura
	
	[FUNCAO LEITURA]
	exemplo.ppm (ENTER)   --   caso a imagem não esteja na mesma pasta que o projeto colocar o link completo
	
	[SEGUNDO MENU]
	2 (ENTER)  --  entra na função de gravura
	
	[FUNCAO PROCURAR MENSAGEM ESCONDIDA]
	s (ENTER)  --  o programa pede pro usuário verificar o comentário da foto e ver se atende os requisitos
	(ENTER)  --  a aplicação pede que de um enter para continuar
	
	[SEGUNDO MENU]
	0 (ENTER)  --  para sair do programa
	(ENTER)  --  a aplicação pede que de um enter para continuar
```
