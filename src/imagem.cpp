#include "imagem.hpp"
#include <iostream>
#include <string>

using namespace std;

Imagem::Imagem(){
  tipo = "nenhum";
  comentario = "nenhum";
  largura = 0;
  altura = 0;
  valorMaximo = 0;
}

Imagem::~Imagem(){
  
}

void Imagem::setTipo (string tipo){
  this->tipo = tipo;
}

string Imagem::getTipo (){
  return tipo;
}

void Imagem::setComentario (string comentario){
  this->comentario = comentario;
}

string Imagem::getComentario (){
  return comentario;
}

void Imagem::setLargura (int largura){
  this->largura = largura;
}

int Imagem::getLargura (){
  return largura;
}

void Imagem::setAltura (int altura){
  this->altura = altura;
}

int Imagem::getAltura (){
  return altura;
}

void Imagem::setValorMaximo (int valorMaximo){
  this->valorMaximo = valorMaximo;
}

int Imagem::getValorMaximo (){
  return valorMaximo;
}
