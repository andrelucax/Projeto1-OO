#include "ppm.hpp"
#include <cstring>
#include <fstream>
#include <cstdlib>

#include <iostream>
using namespace std;

Ppm::Ppm(){

}

Ppm::~Ppm(){

}

void Ppm::setPixelsR (int **pixelsR){
  this->pixelsR=pixelsR;
}

int** Ppm::getPixelsR (){
  return pixelsR;
}

void Ppm::setPixelsG (int **pixelsG){
  this->pixelsG=pixelsG;
}

int** Ppm::getPixelsG (){
  return pixelsG;
}

void Ppm::setPixelsB (int **pixelsB){
  this->pixelsB=pixelsB;
}

int** Ppm::getPixelsB (){
  return pixelsB;
}

Ppm Ppm::leitura(string endereco){
  Ppm imagemPpm;
  ifstream imagem;
  imagem.open(endereco.c_str());
  if(!imagem.good() || (endereco.c_str()[strlen(endereco.c_str())-1] != 'm' &&
  endereco.c_str()[strlen(endereco.c_str())-2] != 'p' &&
  endereco.c_str()[strlen(endereco.c_str())-3] != 'p' &&
  endereco.c_str()[strlen(endereco.c_str())-4] != '.')){
    return imagemPpm;
  }
  else{
    string tipo;
    imagem >> tipo;
    imagemPpm.setTipo(tipo);
    string comentario;
    getline(imagem, comentario);
    getline(imagem, comentario);
    if (comentario.c_str()[0] == '#'){ // verifica existencia de comentario
      imagemPpm.setComentario(comentario);
    }
    else{
      imagem.seekg(0);
      imagem >> tipo;
      getline(imagem, comentario); // pega /n que nao foi pego
    }
    int largura;
    imagem >> largura;
    imagemPpm.setLargura(largura);
    int altura;
    imagem >> altura;
    imagemPpm.setAltura(altura);
    int valorMaximo;
    imagem >> valorMaximo;
    imagemPpm.setValorMaximo(valorMaximo);

    // alocacao dinamina de uma matriz de interios
    int **pixelsR;
    int **pixelsG;
    int **pixelsB;
    pixelsR = (int**) malloc(altura*sizeof(int*));
    pixelsG = (int**) malloc(altura*sizeof(int*));
    pixelsB = (int**) malloc(altura*sizeof(int*));
    for (int aux1=0; aux1<altura; aux1++) {
        pixelsR[aux1] = (int*) malloc(largura*sizeof(int));
        pixelsG[aux1] = (int*) malloc(largura*sizeof(int));
        pixelsB[aux1] = (int*) malloc(largura*sizeof(int));
    }

    if (imagemPpm.getTipo() == "P6"){
      imagem.get(); // pega \n pra não atrapalhar leitura de pixels caso imagem seja em binario
    }

    for (int aux1=0;aux1<altura;aux1++){
      for(int aux2=0;aux2<largura;aux2++){
        if(imagemPpm.getTipo() == "P6"){ // binario
          char aux;
          imagem.get(aux);
          pixelsR[aux1][aux2] = (int)aux;
          imagem.get(aux);
          pixelsG[aux1][aux2] = (int)aux;
          imagem.get(aux);
          pixelsB[aux1][aux2] = (int)aux;
        }
        else if(imagemPpm.getTipo() == "P3"){ // decimal
          int aux;
          imagem >> aux;
          pixelsR[aux1][aux2] = aux;
          imagem >> aux;
          pixelsG[aux1][aux2] = aux;
          imagem >> aux;
          pixelsB[aux1][aux2] = aux;
        }
      }
    }
    imagemPpm.setPixelsR(pixelsR);
    imagemPpm.setPixelsG(pixelsG);
    imagemPpm.setPixelsB(pixelsB);
    imagem.close();
  }
  return imagemPpm;
}

void Ppm::gravura (Ppm imagemPpm, string endereco){
  ofstream escreve;
  escreve.open(endereco.c_str());
  escreve << imagemPpm.getTipo() << endl;
  escreve << "# Imagem PPM recriada por Andre Lucas" << endl;
  escreve << imagemPpm.getLargura() << " ";
  escreve << imagemPpm.getAltura() << endl;
  escreve << imagemPpm.getValorMaximo() << endl;

  for (int aux1=0;aux1<imagemPpm.getAltura();aux1++){
    for(int aux2=0;aux2<imagemPpm.getLargura();aux2++){
      if (imagemPpm.getTipo() == "P3"){ // decimal
        escreve << imagemPpm.getPixelsR()[aux1][aux2] << " ";
        escreve << imagemPpm.getPixelsG()[aux1][aux2] << " ";
        escreve << imagemPpm.getPixelsB()[aux1][aux2] << " ";
      }
      else if (imagemPpm.getTipo() == "P6"){ // binario
        escreve << (char)imagemPpm.getPixelsR()[aux1][aux2];
        escreve << (char)imagemPpm.getPixelsG()[aux1][aux2];
        escreve << (char)imagemPpm.getPixelsB()[aux1][aux2];
      }
    }
  }
  escreve.close();
}

void Ppm::descriptografar(Ppm imagemPpm){
  if (imagemPpm.getComentario() != "nenhum"){
    int comeco;
    int tamanhoDaMensagem;
    string palavraChave;
    ifstream coloca;
    FILE * tempFile;
    tempFile = fopen ("temp.dat", "w");
    fputs(imagemPpm.getComentario().c_str(), tempFile);
    coloca.open("temp.dat");
    rewind(tempFile);
    fclose(tempFile);
    remove("temp.dat");
    coloca.get();// pega # que nao vai ser utilizado
    coloca >> comeco;
    coloca >> tamanhoDaMensagem;
    coloca >> palavraChave;
    coloca.close();

    char alfabeto[] = {' ','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
    char novoAlfabeto[] = {' ','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'};
    char proximoChar = 'a';

    for(unsigned int aux1 = 1; aux1<27; aux1++){
      if(aux1 <= strlen(palavraChave.c_str())){
        novoAlfabeto[aux1] = palavraChave.c_str()[aux1-1];
      }
      else{
        for (int aux2 = 1; aux2<27; aux2++){
          if (novoAlfabeto[aux2] == proximoChar){
            proximoChar++;
            aux2 = 0;
          }
        }
        novoAlfabeto[aux1] = proximoChar;
      }
    }
    cout << "A mensagem escondida na imagem e: \"";
    int contador = 0;
    int somaDosPixels = 0;
    int contadorDeSoma = 0;
    for (int aux1 = 0; aux1 < imagemPpm.getAltura(); aux1++){
      for (int aux2 = 0; aux2 < imagemPpm.getLargura(); aux2++){
        for (int aux4 = 0; aux4 < 3; aux4++){
          if (contador >= comeco){
            if (contador - comeco >= tamanhoDaMensagem*3){
              break;
            }
            if (aux4 == 0){ // R
              contadorDeSoma++;
              somaDosPixels = somaDosPixels + (int)(unsigned char)imagemPpm.getPixelsR()[aux1][aux2]%10;
            }
            else if (aux4 == 1){ // G
              contadorDeSoma++;
              somaDosPixels = somaDosPixels + (int)(unsigned char)imagemPpm.getPixelsG()[aux1][aux2]%10;
            }
            else if (aux4 == 2){ // B
              contadorDeSoma++;
              somaDosPixels = somaDosPixels + (int)(unsigned char)imagemPpm.getPixelsB()[aux1][aux2]%10;
            }
          }
          if (contadorDeSoma == 3){
            contadorDeSoma = 0;
            for (int aux3 = 0; aux3 < 27; aux3++){
              if (alfabeto[somaDosPixels] == novoAlfabeto[aux3]){
                cout << alfabeto[aux3];
              }
            }
            somaDosPixels = 0;
          }
          contador++;
        }
      }
    }
    cout << "\"" << endl;
  }
}
