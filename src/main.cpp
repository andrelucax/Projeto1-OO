#include "menu.hpp"
#include <iostream>
#include <cstdlib>
#include "pgm.hpp"
#include "ppm.hpp"

using namespace std;

char interacaoPGM(char opcao);
char interacaoPPM(char opcao);
void retornandoAoMenuInicial();
void retornandoAoSegundoMenu();

int main (int argc, char const *argv[]){

  menuInicial();
  char opcao;
  do{
    if(opcao == '0'){
      break;
    }
    cin >> opcao;
    if (opcao == '1'){ // leitura PGM
      opcao =  interacaoPGM(opcao);
    }
    else if (opcao == '2'){ // leitura PPM
      opcao =  interacaoPPM(opcao);
    }
    else if (opcao == '0'){ // sair do programa
      system("clear");
      cout << "Obrigado por usar o programa, aperte enter para sair";
      string aux;
      getline(cin, aux); // Pega \n que nao foi utilizado
      getline(cin, aux);
      break;
    }
    else{
      cout << "Opcao invalida, digite novamente: ";
    }
  }while(1);
  return 0;
}

char interacaoPGM(char opcao){
  Pgm imagemPgm;
  bemVindo();
  cout << "Funcao leitura, digite o endereço da imagem (coloque \".pgm\" no final): ";
  string endereco;
  cin >> endereco;
  imagemPgm = imagemPgm.leitura(endereco);
  if (imagemPgm.getTipo() == "P5" || imagemPgm.getTipo() == "P2"){
    do{
      segundoMenuPGM();
      cin >> opcao;
      if (opcao == '1'){
        bemVindo();
        cout << "Funcao gravura, digite o nome que deseja dar a sua imagem (coloque \".pgm\" no final): ";
        cin >> endereco;
        imagemPgm.gravura(imagemPgm, endereco);
        cout << "Imagem salva com sucesso";
        retornandoAoSegundoMenu();
      }
      else if (opcao == '2'){
        bemVindo();
        cout << "Funcao procurar mensagem escondida" << endl;
        cout << "O comentario da sua foto e: \"";
        cout << imagemPgm.getComentario() << "\"" << endl;
        cout << "O comentario deve ser do tipo \"#comeco(inteiro) tamanho(inteiro) cifra(inteiro)\"" << endl;
        cout << "Deseja continuar (s/any)? ";
        char opcao2;
        cin >> opcao2;
        if (opcao2 == 's' || opcao2 == 'S'){
          imagemPgm.descriptografar(imagemPgm);
        }
        retornandoAoSegundoMenu();
      }
      else if (opcao == '9'){
        retornandoAoMenuInicial();
        break;
      }
      else if (opcao == '0'){
        system("clear");
        cout << "Obrigado por usar o programa, aperte enter para sair";
        string aux;
        getline(cin, aux); // Pega \n que nao foi utilizado
        getline(cin, aux);
        break;
      }
      else{
        cout << "Opcao invalida, digite novamente: ";
        cin.get(); // Pega \n que nao foi utilizado
      }
    }while(1);
  }
  else{
    cout << "Endereco invalido.";
    retornandoAoMenuInicial();
  }
  return opcao;
}

char interacaoPPM(char opcao){
  Ppm imagemPpm;
  bemVindo();
  cout << "Funcao leitura, digite o endereço da imagem (coloque \".ppm\" no final): ";
  string endereco;
  cin >> endereco;
  imagemPpm = imagemPpm.leitura(endereco);
  if (imagemPpm.getTipo() == "P6" || imagemPpm.getTipo() == "P3"){
    do{
      segundoMenuPPM();
      cin >> opcao;
      if (opcao == '1'){
        bemVindo();
        cout << "Funcao gravura, digite o nome que deseja dar a sua imagem (coloque \".ppm\" no final): ";
        cin >> endereco;
        imagemPpm.gravura(imagemPpm, endereco);
        cout << "Imagem salva com sucesso.";
        retornandoAoSegundoMenu();
      }
      else if (opcao == '2'){
        bemVindo();
        cout << "Funcao procurar mensagem escondida" << endl;
        cout << "O comentario da sua foto e: \"";
        cout << imagemPpm.getComentario() << "\"" << endl;
        cout << "O comentario deve ser do tipo \"#comeco(inteiro) tamanho(inteiro) keyword(string)\"" << endl;
        cout << "Deseja continuar (s/any)? ";
        char opcao2;
        cin >> opcao2;
        if (opcao2 == 's' || opcao2 == 'S'){
          imagemPpm.descriptografar(imagemPpm);
        }
        retornandoAoSegundoMenu();
      }
      else if (opcao == '9'){
        retornandoAoMenuInicial();
        break;
      }
      else if (opcao == '0'){
        system("clear");
        cout << "Obrigado por usar o programa, aperte enter para sair";
        string aux;
        getline(cin, aux); // Pega \n que nao foi utilizado
        getline(cin, aux);
        break;
      }
      else{
        cout << "Opcao invalida, digite novamente: ";
      }
    }while(1);
  }
  else{
    cout << "Endereco invalido."; //, retornando ao menu inical, aperte enter para continuar";
    retornandoAoMenuInicial();
  }
  return opcao;
}

void retornandoAoMenuInicial(){
  cout << endl << "Retornando ao menu inicial, aperte enter para continuar";
  string aux;
  getline(cin, aux); // Pega \n que nao foi utilizado
  getline(cin, aux);
  menuInicial();
}

void retornandoAoSegundoMenu(){
  cout << endl << "Retornando ao segundo menu, aperte enter para continuar";
  string aux;
  getline(cin, aux); // Pega \n que nao foi utilizado
  getline(cin, aux);
}
