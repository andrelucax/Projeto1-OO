#include "menu.hpp"
#include <iostream>
#include <cstdlib>

using namespace std;

void bemVindo(){
  system("clear");
  cout << "Bem vindo!" << endl << endl;
}

void menuInicial(){
  bemVindo();
  cout << "Menu inicial" << endl << endl;
  cout << "Opcao 1: leitura de imagem PGM;" << endl;
  cout << "Opcao 2: leitura de imagem PPM;" << endl;
  cout << "Opcao 0: sair do programa." << endl << endl;
  cout << "Digite a opcao (numerica) que deseja utilizar: ";
}

void segundoMenuPGM(){
  bemVindo();
  cout << "Segundo menu, imagem PGM:" << endl << endl;
  cout << "Opcao 1: gravura de imagem PGM;" << endl;
  cout << "Opcao 2: procurar mensagem escondida;" << endl;
  cout << "Opcao 9: voltar ao menu inicial;" << endl;
  cout << "Opcao 0: sair do programa." << endl << endl;
  cout << "Digite a opcao (numerica) que deseja utilizar: ";
}

void segundoMenuPPM(){
  bemVindo();
  cout << "Segundo menu, imagem PPM:" << endl << endl;
  cout << "Opcao 1: gravura de imagem PPM;" << endl;
  cout << "Opcao 2: procurar mensagem escondida;" << endl;
  cout << "Opcao 9: voltar ao menu inicial;" << endl;
  cout << "Opcao 0: sair do programa." << endl << endl;
  cout << "Digite a opcao (numerica) que deseja utilizar: ";
}
