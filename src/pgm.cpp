#include "pgm.hpp"
#include <cstdlib>
#include <fstream>
#include <cstring>

#include <iostream>
using namespace std;

Pgm::Pgm(){

}

Pgm::~Pgm(){

}

void Pgm::setPixels (int **pixels){
  this->pixels = pixels;
}


int** Pgm::getPixels (){
  return pixels;
}

Pgm Pgm::leitura(string endereco){
  Pgm imagemPgm;
  ifstream imagem;
  imagem.open(endereco.c_str());
  if(!imagem.good() || (endereco.c_str()[strlen(endereco.c_str())-1] != 'm' &&
  endereco.c_str()[strlen(endereco.c_str())-2] != 'g' &&
  endereco.c_str()[strlen(endereco.c_str())-3] != 'p' &&
  endereco.c_str()[strlen(endereco.c_str())-4] != '.')){
    return imagemPgm;
  }
  else{
    string tipo;
    imagem >> tipo;
    imagemPgm.setTipo(tipo);
    string comentario;
    getline(imagem, comentario);
    getline(imagem, comentario);
    if (comentario.c_str()[0] == '#'){ // verifica existencia de comentario
      imagemPgm.setComentario(comentario);
    }
    else{
      imagem.seekg(0);
      imagem >> tipo;
      //getline(imagem, comentario); // pega /n que nao foi pego
      imagem.get(); // pega /n que nao foi pego
    }
    int largura;
    imagem >> largura;
    imagemPgm.setLargura(largura);
    int altura;
    imagem >> altura;
    imagemPgm.setAltura(altura);
    int valorMaximo;
    imagem >> valorMaximo;
    imagemPgm.setValorMaximo(valorMaximo);

    // alocacao dinamina de uma matriz de interios
    int **pixels;
    pixels = (int**) malloc(altura*sizeof(int*));
    for (int aux1=0; aux1<altura; aux1++) {
        pixels[aux1] = (int*) malloc(largura*sizeof(int));
    }

    if (imagemPgm.getTipo() == "P5"){
      imagem.get(); // pega \n pra não atrapalhar leitura de pixels caso imagem seja em binario
    }

    for (int aux1=0;aux1<altura;aux1++){
      for(int aux2=0;aux2<largura;aux2++){
        if(imagemPgm.getTipo() == "P5"){ // binario
          char aux;
          imagem.get(aux);
          pixels[aux1][aux2] = (int)aux;
        }
        else if(imagemPgm.getTipo() == "P2"){ // decimal
          int aux;
          imagem >> aux;
          pixels[aux1][aux2] = aux;
        }
      }
    }
    imagemPgm.setPixels(pixels);
    imagem.close();
  }
  return imagemPgm;
}

void Pgm::gravura (Pgm imagemPgm, string endereco){
  ofstream escreve;
  escreve.open(endereco.c_str());
  escreve << imagemPgm.getTipo() << endl;
  escreve << "# Imagem PGM recriada por Andre Lucas" << endl;
  escreve << imagemPgm.getLargura() << " ";
  escreve << imagemPgm.getAltura() << endl;
  escreve << imagemPgm.getValorMaximo() << endl;

  for (int aux1=0;aux1<imagemPgm.getAltura();aux1++){
    for(int aux2=0;aux2<imagemPgm.getLargura();aux2++){
      if (imagemPgm.getTipo() == "P2"){ // decimal
        escreve << imagemPgm.getPixels()[aux1][aux2] << " ";
      }
      else if (imagemPgm.getTipo() == "P5"){ // binario
        escreve << (char)imagemPgm.getPixels()[aux1][aux2];
      }
    }
  }
  escreve.close();
}

void Pgm::descriptografar(Pgm imagemPgm){
  if (imagemPgm.getComentario() != "nenhum"){
    int comeco;
    int tamanhoDaMensagem;
    int cifra;
    ifstream coloca;
    FILE * tempFile;
    tempFile = fopen ("temp.dat", "w");
    fputs(imagemPgm.getComentario().c_str(), tempFile);
    coloca.open("temp.dat");
    rewind(tempFile);
    fclose(tempFile);
    remove("temp.dat");
    coloca.get();// pega # que nao vai ser utilizado
    coloca >> comeco;
    coloca >> tamanhoDaMensagem;
    coloca >> cifra;
    coloca.close();

    cifra = cifra%26;
    cout << "A mensagem escondida na mensagem e: \"";
    int contador = 0;
    for (int aux1 = 0; aux1 < imagemPgm.getAltura(); aux1++){
      for (int aux2 = 0; aux2 < imagemPgm.getLargura(); aux2++){
        if (contador >= comeco){
          if (contador - comeco >= tamanhoDaMensagem){
            break;
          }
          if (((char)pixels[aux1][aux2] >= (97 + cifra)) && ((char)pixels[aux1][aux2] <= (122)))
          {
            cout << (char)(pixels[aux1][aux2] - cifra);
          }
          else if (((char)pixels[aux1][aux2] >= (97)) && ((char)pixels[aux1][aux2] <= ((97 + cifra))))
          {
            cout << (char)(122 - cifra + pixels[aux1][aux2] - 96);
          }
          else if (((char)pixels[aux1][aux2] >= (65 + cifra)) && ((char)pixels[aux1][aux2] <= (90)))
          {
            cout << (char)(pixels[aux1][aux2] - cifra);
          }
          else if (((char)pixels[aux1][aux2] >= (65)) && ((char)pixels[aux1][aux2] <= (65 + cifra)))
          {
            cout << (char)(90 - cifra + pixels[aux1][aux2] - 64);
          }
          else {
            cout << (char)pixels[aux1][aux2];
          }
        }
        contador++;
      }
    }

    cout << "\"" << endl;
  }
}
