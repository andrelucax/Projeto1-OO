#ifndef PPM_HPP
#define PPM_HPP

#include "imagem.hpp"

class Ppm : public Imagem{
  // atributos
  private:
    int **pixelsR;
    int **pixelsG;
    int **pixelsB;

  // metodos
  public:
    Ppm();
    ~Ppm();

    void setPixelsR (int **pixelsR);
    int** getPixelsR ();

    void setPixelsG (int **pixelsG);
    int** getPixelsG ();

    void setPixelsB (int **pixelsB);
    int** getPixelsB ();

    Ppm leitura(string endereco);
    void gravura (Ppm imagemPpm, string endereco);

    void descriptografar(Ppm imagemPpm);
};

#endif
