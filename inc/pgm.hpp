#ifndef PGM_HPP
#define PGM_HPP

#include "imagem.hpp"

class Pgm : public Imagem{
  // atributos
  private:
    int **pixels;

  // metodos
  public:
    Pgm();
    ~Pgm();

    void setPixels (int **pixels);
    int** getPixels ();

    Pgm leitura(string endereco);
    void gravura (Pgm imagemPgm, string endereco);

    void descriptografar(Pgm imagemPgm);
};

#endif
