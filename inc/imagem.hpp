#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>

using namespace std;

class Imagem{
  // atributos
  private:
    string tipo;
    string comentario;
    int largura;
    int altura;
    int valorMaximo;

  // metodos
  public:
    Imagem();
    ~Imagem();

    void setTipo (string tipo);
    string getTipo ();

    void setComentario (string comentario);
    string getComentario ();

    void setLargura (int largura);
    int getLargura ();

    void setAltura (int altura);
    int getAltura ();

    void setValorMaximo (int valorMaximo);
    int getValorMaximo ();
};

#endif
